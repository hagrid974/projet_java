package com.epf.RentManager.service;

import java.util.List;
import java.util.Optional;

import com.epf.RentManager.exception.DaoException;
import com.epf.RentManager.exception.ServiceException;
import com.epf.RentManager.model.Vehicule;
import com.epf.RentManager.dao.VehiculeDao;

public class VehiculeService {

	private VehiculeDao vehiculedao;
	public static VehiculeService instance;
	
	private VehiculeService() {
		this.vehiculedao = VehiculeDao.getInstance();
	}
	
	public static VehiculeService getInstance() {
		if (instance == null) {
			instance = new VehiculeService();
		}
		
		return instance;
	}
	
	
	public long create(Vehicule vehicule) throws ServiceException {
		// TODO: creer un vehicule
		
		if(vehicule.getNb_place() < 2 || vehicule.getNb_place() > 9) {
			throw new ServiceException("Le nombre de places doit être compris entre 2 et 9.");
		}
		
		vehicule.setConstructeur(vehicule.getConstructeur().toUpperCase());
		vehicule.setModele(vehicule.getModele().substring(0, 1).toUpperCase() + vehicule.getModele().substring(1));
		
		try {
			return vehiculedao.create(vehicule);
		} catch (DaoException e) {
			throw new ServiceException(e.getMessage());
		}
		
	}

	public Optional<Vehicule> findById(int id) throws ServiceException {
		// TODO: recuperer un vehicule par son id
		
		Optional<Vehicule> optVehicule = vehiculedao.findById(id);
		
		//Uniquement pour le CliControler
		if(optVehicule.isPresent()) {
			Vehicule v = optVehicule.get();
			System.out.println(v);
		}else {
			System.out.println("Erreur lors du select du vehicule");
		}
		//Fin de la partie pour le CliControler
		
		return optVehicule;
		
	}

	public List<Vehicule> findAll() throws ServiceException {
		// TODO: recuperer tous les vehicules
		
		try {
			return vehiculedao.findAll();
		} catch (DaoException e) {
			throw new ServiceException (e.getMessage());
		}
		
	}
	
	public long delete (int id) throws ServiceException {
		
		try {
			return vehiculedao.delete(id);
		} catch (DaoException e) {
			throw new ServiceException (e.getMessage());
		}
	}
}
