package com.epf.RentManager.service;

import java.time.temporal.ChronoUnit;
import java.util.List;

import com.epf.RentManager.dao.ReservationDao;
import com.epf.RentManager.exception.DaoException;
import com.epf.RentManager.exception.ServiceException;
import com.epf.RentManager.model.Reservation;

	public class ReservationService {
	
	private ReservationDao resadao;
	public static ReservationService instance;
	
	private ReservationService() {
		this.resadao = ReservationDao.getInstance();
	}
	
	public static ReservationService getInstance() {
		if (instance == null) {
			instance = new ReservationService();
		}
		
		return instance;
	}

	public long create(Reservation resa) throws ServiceException {
		// TODO: creer une reservation		
		
		long limitUser = ChronoUnit.DAYS.between(resa.getDebut().toLocalDate(), resa.getFin().toLocalDate());
		if(limitUser > 6) {
			throw new ServiceException("Une voiture ne peut pas être réservée plus de 7 jours.");
		}
		
		try {
			return resadao.create(resa);
		} catch (DaoException e) {
			throw new ServiceException(e.getMessage());
		}
	}
	
	public List<Reservation> findResaByClientId(int clientId) throws ServiceException {
		// TODO: recuperer une reservation avec l'id du client
		
		try {
			return resadao.findResaByClientId(clientId);
		}
		catch (DaoException e) {
			throw new ServiceException (e.getMessage());
		}
	}
	
	public List<Reservation> findResaByVehicleId(int vehiculeId) throws ServiceException {
		// TODO: recuperer une reservation avec l'id du véhicule
		
		try {
			return resadao.findResaByVehicleId(vehiculeId);
		}
		catch (DaoException e) {
			throw new ServiceException (e.getMessage());
		}
	}
	
	public List<Reservation> findAll() throws ServiceException {
		// TODO: recuperer toutes les reservations	
		try {
			return resadao.findAll();
		} catch (DaoException e) {
			throw new ServiceException (e.getMessage());
		}
	}
	
	public long delete (int id) throws ServiceException {
		
		try {
			return resadao.delete(id);
		} catch (DaoException e) {
			throw new ServiceException (e.getMessage());
		}
	}
}
