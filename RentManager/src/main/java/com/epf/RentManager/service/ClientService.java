package com.epf.RentManager.service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import com.epf.RentManager.exception.DaoException;
import com.epf.RentManager.exception.ServiceException;
import com.epf.RentManager.model.Client;
import com.epf.RentManager.dao.ClientDao;

public class ClientService {
	
	private ClientDao clientdao;
	public static ClientService instance;
	
	private ClientService() {
		this.clientdao = ClientDao.getInstance();
	}
	
	public static ClientService getInstance() {
		if (instance == null) {
			instance = new ClientService();
		}
		
		return instance;
	}
	
	
	public long create(com.epf.RentManager.model.Client client) throws ServiceException {
		
		long age = ChronoUnit.YEARS.between(client.getNaissance().toLocalDate(), LocalDate.now());
		if(age < 18) {
			throw new ServiceException("Le client doit avoir 18 ans.");
		}
		
		client.setNom(client.getNom().toUpperCase());
		client.setPrenom(client.getPrenom().substring(0, 1).toUpperCase() + client.getPrenom().substring(1).toLowerCase());
		client.setEmail(client.getEmail().toLowerCase());
		
		try {
			List<Client> list = clientdao.findAll();
			
			for(Client clientList : list) {
				if (clientList.getEmail().equals(client.getEmail())) {
					throw new ServiceException("Cette adresse mail est déjà prise.");
				}
			}
			
			return clientdao.create(client);
		} catch (DaoException e) {
			throw new ServiceException(e.getMessage());
		}
	}
	
	public Optional<Client> findById(int id) throws ServiceException {
		// TODO: recuperer un client par son id
		
		Optional<Client> optClient = clientdao.findById(id);
		
		//Uniquement pour le CliControler
		if(optClient.isPresent()) {
			Client c = optClient.get();
			System.out.println(c);
		}else {
			System.out.println("Erreur lors du select du vehicule");
		}
		//Fin de la partie pour le CliControler
		
		return optClient;
		
		
	}
	
	public long delete(int id) throws ServiceException {
		// TODO: supprimer un client par son id
		
		try {	
			return clientdao.delete(id);
		} catch (DaoException e) {
			throw new ServiceException (e.getMessage());
		}
	}

	public List<Client> findAll() throws ServiceException {
		// TODO: recuperer tous les clients
		try {
			return clientdao.findAll();
		} catch (DaoException e) {
			throw new ServiceException (e.getMessage());
		}
		
	}
	
}
