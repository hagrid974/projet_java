package com.epf.RentManager.validator;

import com.epf.RentManager.exception.InputException;
import com.epf.RentManager.model.Vehicule;

public class VehiculeValidator extends Vehicule {
	
	public VehiculeValidator() {
		super();
	}
	
	public VehiculeValidator(int id, String constructeur, String modele, byte nb_place) {
		super(id, constructeur, modele, nb_place);
	}
	
	public void validator() throws InputException {
		
		if(super.getConstructeur().trim().length() < 2) {
			throw new InputException("Le champ marque doit contenir au moins 2 caractères.");
		}
		if("".equals(super.getModele().trim())) {
			throw new InputException("Le champ modele est vide.");
		}
		
	}

}
