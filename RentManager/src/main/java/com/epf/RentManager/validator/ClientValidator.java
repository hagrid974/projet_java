package com.epf.RentManager.validator;

import java.sql.Date;

import com.epf.RentManager.exception.InputException;
import com.epf.RentManager.model.Client;

public class ClientValidator extends Client {
	
	public ClientValidator() {	
		super();
	}
	public ClientValidator(int id, String nom, String prenom, String email, Date naissance) {
		super(id, nom, prenom, email, naissance);
	}
	
	public void validator() throws InputException {
	
		if (super.getNom().trim().length() < 3) {
			throw new InputException("Le champ nom doit contenir au moins 3 caractères.");
		}
		if (super.getPrenom().trim().length() < 3) {
			throw new InputException("Le champ prénom doit contenir au moins 3 caractères.");
		}
		if (super.getEmail().matches("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$") == false) {
			throw new InputException("Veuillez saisir un email valide");
		}
	}


}
