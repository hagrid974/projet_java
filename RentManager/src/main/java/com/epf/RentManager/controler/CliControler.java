package com.epf.RentManager.controler;

import java.sql.Date;
import java.util.List;
import java.util.Scanner;

import com.epf.RentManager.exception.InputException;
import com.epf.RentManager.exception.ServiceException;
import com.epf.RentManager.model.Client;
import com.epf.RentManager.model.Reservation;
import com.epf.RentManager.model.Vehicule;
import com.epf.RentManager.service.ClientService;
import com.epf.RentManager.service.ReservationService;
import com.epf.RentManager.service.VehiculeService;
import com.epf.RentManager.validator.ClientValidator;
import com.epf.RentManager.validator.VehiculeValidator;

public class CliControler {
	
	private ClientService clientService = ClientService.getInstance();
	private VehiculeService vehiculeService = VehiculeService.getInstance();
	private ReservationService reservationService = ReservationService.getInstance();
	
	public static void main (String[] args) {
		
		CliControler cli = new CliControler();
		Scanner sc = new Scanner(System.in);
		boolean done = false;
		while(!done) {
		
			System.out.println("Liste des opérations");
			System.out.println("0 - Quitter le programme");
			System.out.println("1 - Afficher la liste des clients");
			System.out.println("2 - Afficher la liste des véhicules");
			System.out.println("3 - Afficher un client donné");
			System.out.println("4 - Afficher un véhicule donné");
			System.out.println("5 - Ajouter un client");
			System.out.println("6 - Ajouter un véhicule");
			System.out.println("7 - Supprimer un client");
			System.out.println("8 - Supprimer un véhicule");
			System.out.println("9 - Faire une réservation");
			System.out.println("10 - Afficher toutes les réservations");
			System.out.println("11 - Afficher toutes les réservations d'un client donné");
			System.out.println("12 - Afficher toutes les réservations d'un véhicule donné");
			System.out.println("13 - Supprimer une réservation");
			
			int choix = sc.nextInt();
			sc.nextLine();
			
			switch (choix) {
			case 0:
				done = true;
				break;
			case 1:
				printAllClients(cli);
				break;
			case 2:
				printAllVehicles(cli);
				break;
			case 3:
				findClientByID(cli, sc);
				break;
			case 4:
				findVehicleByID(cli, sc);
				break;
			case 5:
				addClient(cli, sc);
				break;
			case 6:
				addVehicle(cli, sc);
				break;
			case 7:
				deleteClient(cli, sc);
				break;
			case 8:
				deleteVehicle(cli, sc);
				break;
			case 9:
				addReservation(cli, sc);
				break;
			case 10:
				printAllReservations(cli);
				break;
			case 11:
				findResaByClient(cli, sc);
				break;
			case 12:
				findResaByVehicle(cli, sc);
				break;
			case 13:
				deleteReservation(cli, sc);
				break;
			default:
				System.out.println("PAS LE BON CHOIX");
			}
		}
		sc.close();
	}
	
	private static void printAllClients(CliControler cli) {
		try {
			List<Client> list = cli.clientService.findAll();
			
			for(Client client : list) {
				System.out.println(client);
			}
			
			//Alternative plus compliquee   
			//list.stream().forEach(System.out::println);
			
		} catch (ServiceException e) {
			System.out.println("Une erreur est survenue :" + e.getMessage());
		}
	}
	
	private static void printAllVehicles(CliControler cli) {
		try {
			List<Vehicule> list = cli.vehiculeService.findAll();
			
			for(Vehicule vehicule : list) {
				System.out.println(vehicule);
			}
			
			//Alternative plus compliquee   
			//list.stream().forEach(System.out::println);
			
		} catch (ServiceException e) {
			System.out.println("Une erreur est survenue :" + e.getMessage());
		}
	}
	
	private static void printAllReservations(CliControler cli) {
		try {
			List<Reservation> list = cli.reservationService.findAll();
			
			for(Reservation reservation : list) {
				System.out.println(reservation);
			}
			
		} catch (ServiceException e) {
			System.out.println("Une erreur est survenue :" + e.getMessage());
		}
	}
	
	private static void addClient(CliControler cli, Scanner sc) {
		Client client = new Client();
		
		try {
			System.out.println("Entrez le nom");
			client.setNom(sc.nextLine());
			System.out.println("Entrez le prénom");
			client.setPrenom(sc.nextLine());
			System.out.println("Entrez l'email");
			client.setEmail(sc.nextLine());
			System.out.println("Entrez la date au format yyyy-[m]m-[d]d");
			client.setNaissance(Date.valueOf(sc.nextLine()));
			
			ClientValidator clientValidator = new ClientValidator(client.getId(), client.getNom(), client.getPrenom(), client.getEmail(), client.getNaissance());
			clientValidator.validator();
			
			cli.clientService.create(client);
		} catch (InputException e) {
			System.out.println("Une erreur est survenue :" + e.getMessage());
		} catch (ServiceException e) {
			System.out.println("Une erreur est survenue :" + e.getMessage());
		} catch (IllegalArgumentException e) {
			System.out.println("Veuillez saisir une date valide (yyyy-mm-dd)");
		} 
	}
	
	private static void addVehicle(CliControler cli, Scanner sc) {
		Vehicule vehicule = new Vehicule();
		
		try {
			
			System.out.println("Entrez le constructeur");
			vehicule.setConstructeur(sc.nextLine());
			System.out.println("Entrez le modele");
			vehicule.setModele(sc.nextLine());
			System.out.println("Entrez le nombre de places");
			vehicule.setNb_place(sc.nextByte());
			
			VehiculeValidator vehiculeValidator = new VehiculeValidator(vehicule.getId(), vehicule.getConstructeur(), vehicule.getModele(), vehicule.getNb_place());
			vehiculeValidator.validator();
			
			cli.vehiculeService.create(vehicule);
		} catch (InputException e) {
			System.out.println("Une erreur est survenue :" + e.getMessage());
		} catch (ServiceException e) {
			System.out.println("Une erreur est survenue :" + e.getMessage());
		}
	}
	
	private static void addReservation(CliControler cli, Scanner sc) {
		Reservation resa = new Reservation();
		
		try {
			
			System.out.println("Entrez l'identifiant du client");
			int client_id = sc.nextInt();
			
			List<Client> list = cli.clientService.findAll();
			
			for(Client client : list) {
				if (client.getId() == client_id) {
					resa.setClient(client);
				}
			}
		
			System.out.println("Entrez l'identifiant du vehicule");
			int vehicule_id = sc.nextInt();
			
			List<Vehicule> list2 = cli.vehiculeService.findAll();
			
			for(Vehicule vehicule : list2) {
				if(vehicule.getId() == vehicule_id) {
					resa.setVehicule(vehicule);
				}
			}

			System.out.println("Entrez la date de d�but au format yyyy-mm-dd");
			sc.nextLine();
			resa.setDebut(Date.valueOf(sc.nextLine()));
			System.out.println("Entrez la date de fin au format yyyy-mm-dd");
			resa.setFin(Date.valueOf(sc.nextLine()));
	
			cli.reservationService.create(resa);
		}  catch (ServiceException e) {
			System.out.println("Une erreur est survenue : " + e.getMessage());
		} catch (NullPointerException e) {
			System.out.println("Une erreur est survenue : " + e.getMessage());
		} catch (IllegalArgumentException e) {
			System.out.println("Veuillez saisir une date valide (yyyy-mm-dd)");
		}
	}
	
	private static void findClientByID(CliControler cli, Scanner sc) {
		try {	
			System.out.println("Veuillez rentrer l'ID du client que vous souhaitez obtenir :");
			int ID = sc.nextInt();			
			cli.clientService.findById(ID);				
		} catch (ServiceException e) {
			System.out.println("Une erreur est survenue :" + e.getMessage());
		}
	}
	
	private static void findVehicleByID(CliControler cli, Scanner sc) {
		try {	
			System.out.println("Veuillez rentrer l'ID du véhicule que vous souhaitez obtenir :");
			int ID = sc.nextInt();
			cli.vehiculeService.findById(ID);				
		} catch (ServiceException e) {
			System.out.println("Une erreur est survenue :" + e.getMessage());
		}
	}
	
	private static void findResaByClient(CliControler cli, Scanner sc) {
		try {	
			
			System.out.println("Veuillez rentrer l'ID du client concerné :");
			int ID = sc.nextInt();
			List<Reservation> list = cli.reservationService.findResaByClientId(ID);
			
			for(Reservation reservation : list) {
				System.out.println(reservation);
			}
			
		} catch (ServiceException e) {
			System.out.println("Une erreur est survenue :" + e.getMessage());
		}
	}
	
	private static void findResaByVehicle(CliControler cli, Scanner sc) {
		try {	
			
			System.out.println("Veuillez rentrer l'ID du véhicule concerné :");
			int ID = sc.nextInt();	
			List<Reservation> list = cli.reservationService.findResaByVehicleId(ID);	
			
			for(Reservation reservation : list) {
				System.out.println(reservation);
			}
			
		} catch (ServiceException e) {
			System.out.println("Une erreur est survenue :" + e.getMessage());
		}
	}
	
	private static void deleteClient(CliControler cli, Scanner sc) {
		try {
			
			System.out.println("Veuillez rentrer l'ID du client que vous souhaitez supprimer :");
			int ID = sc.nextInt();			
			cli.clientService.delete(ID);
			
		} catch (ServiceException e) {
			System.out.println("Une erreur est survenue :" + e.getMessage());
		}
	}
	
	private static void deleteVehicle(CliControler cli, Scanner sc) {
		try {
			
			System.out.println("Veuillez rentrer l'ID du vehicule que vous souhaitez supprimer :");
			int ID = sc.nextInt();			
			cli.vehiculeService.delete(ID);
			
		} catch (ServiceException e) {
			System.out.println("Une erreur est survenue :" + e.getMessage());
		}
	}
	
	private static void deleteReservation(CliControler cli, Scanner sc) {
		try {
			
			System.out.println("Veuillez rentrer l'ID de la réservation que vous souhaitez supprimer :");
			int ID = sc.nextInt();			
			cli.reservationService.delete(ID);
			
		} catch (ServiceException e) {
			System.out.println("Une erreur est survenue :" + e.getMessage());
		}
	}

}
