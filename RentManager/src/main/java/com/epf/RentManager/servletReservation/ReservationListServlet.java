package com.epf.RentManager.servletReservation;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.RentManager.exception.ServiceException;
import com.epf.RentManager.service.ReservationService;

@WebServlet("/rents")
public class ReservationListServlet extends HttpServlet {
	
	ReservationService resaService = ReservationService.getInstance();
	
	/*
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher ("/WEB-INF/views/rents/list.jsp");
		
		try {
			request.setAttribute("list_reservations", resaService.findAll());
		} catch (ServiceException e) {
			request.setAttribute("list_reservations", "Une erreur est survenue" + e.getMessage());
		}
		dispatcher.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}*/
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		try {
			request.setAttribute("clients", clientService.findAll());
		} catch (ServiceException e) {
			request.setAttribute("clients", "Une erreur est survenue");
		}

		try {
			request.setAttribute("vehicules", vehiculeService.findAll());
		} catch (ServiceException e) {
			request.setAttribute("vehicules", "Une erreur est survenue");
		}

		try {
			request.setAttribute("list_reservations", reservationService.findAll());
		} catch (ServiceException e) {
			request.setAttribute("list_reservations", "Une erreur est survenue");
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher ("/WEB-INF/views/rents/list.jsp");

		dispatcher.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
