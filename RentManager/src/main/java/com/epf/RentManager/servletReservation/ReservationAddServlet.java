package com.epf.RentManager.servletReservation;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.RentManager.exception.ServiceException;
import com.epf.RentManager.model.Client;
import com.epf.RentManager.model.Reservation;
import com.epf.RentManager.model.Vehicule;
import com.epf.RentManager.service.ClientService;
import com.epf.RentManager.service.ReservationService;
import com.epf.RentManager.service.VehiculeService;

@WebServlet("/rents/create")
public class ReservationAddServlet extends HttpServlet{
	
	ReservationService resaService = ReservationService.getInstance();
	ClientService clientService = ClientService.getInstance();
	VehiculeService vehiculeService = VehiculeService.getInstance();

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher ("/WEB-INF/views/rents/create.jsp");
		
		try {
			request.setAttribute("list_clients", clientService.findAll());
			request.setAttribute("list_vehicules", vehiculeService.findAll());
		} catch (ServiceException e) {
			request.setAttribute("errorMessage", "Une erreur est survenue :" + e.getMessage());
		}
		dispatcher.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			
			int client_id = Integer.parseInt(request.getParameter("client"));
			int vehicule_id = Integer.parseInt(request.getParameter("vehicule"));
			Date start = Date.valueOf(request.getParameter("debut"));
			Date end = Date.valueOf(request.getParameter("fin"));
			
			Reservation newResa = new Reservation();
			
			List<Client> list = clientService.findAll();
			
			for(Client client : list) {
				if (client.getId() == client_id) {
					newResa.setClient(client);
				}
			}
			
			List<Vehicule> list2 = vehiculeService.findAll();
			
			for(Vehicule vehicule : list2) {
				if(vehicule.getId() == vehicule_id) {
					newResa.setVehicule(vehicule);
				}
			}
			
			newResa.setDebut(debut);
			newResa.setFin(fin);
			
			resaService.create(newResa);
			response.sendRedirect(request.getContextPath() + "/rents");
			
		} catch (ServiceException e) {
			request.setAttribute("errorMessage", "Une erreur est survenue :" + e.getMessage());
			doGet(request, response);
		} catch (IllegalArgumentException e) {
			request.setAttribute("errorMessage", "Une erreur est survenue : Veuillez saisir une date valide (yyyy-mm-dd)");
			doGet(request, response);
		} 
		
	}*/
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		try {
			request.setAttribute("clients", clientService.findAll());
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			request.setAttribute("vehicules", vehiculeService.findAll());
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/rents/create.jsp");

		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int clientID = Integer.parseInt(request.getParameter("client"));
		int vehiculeID = Integer.parseInt(request.getParameter("car"));
		Date start_date = Date.valueOf(request.getParameter("begin"));
		Date end_date = Date.valueOf(request.getParameter("end"));
		

		Reservation newReservation = new Reservation();
		newReservation.setClient_id(clientID);
		newReservation.setVehicule_id(vehiculeID);
		newReservation.setDebut(start_date);
		newReservation.setFin(end_date);
		RequestDispatcher dispatcher;

		try {
			reservationService.create(newReservation);
			response.sendRedirect(request.getContextPath()+ "/rents");
		} catch (ServiceException e) {
			request.setAttribute("errorMessage", "Une erreur est survenue :" + e.getMessage());
		
			dispatcher = request.getRequestDispatcher("/WEB-INF/views/rents/create.jsp");
			dispatcher.forward(request, response);
		}
	}

}
