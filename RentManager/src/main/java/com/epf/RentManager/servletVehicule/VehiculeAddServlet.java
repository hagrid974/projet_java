package com.epf.RentManager.servletVehicule;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.RentManager.exception.InputException;
import com.epf.RentManager.exception.ServiceException;
import com.epf.RentManager.model.Vehicule;
import com.epf.RentManager.service.VehiculeService;
import com.epf.RentManager.validator.VehiculeValidator;

@WebServlet("/cars/create")
public class VehiculeAddServlet extends HttpServlet{
	
	VehiculeService vehiculeService = VehiculeService.getInstance();

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher ("/WEB-INF/views/vehicles/create.jsp");
		
		dispatcher.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			
			String constructeur = request.getParameter("constructeur");
			String modele = request.getParameter("modele");
			byte nb_place = Byte.parseByte(request.getParameter("nb_place"));

			Vehicule newVehicule = new Vehicule ();
			newVehicule.setConstructeur(constructeur);
			newVehicule.setModele(modele);
			newVehicule.setNb_place(nb_place);
			
			VehiculeValidator vehiculeValidator = new VehiculeValidator(newVehicule.getId(), newVehicule.getConstructeur(), newVehicule.getModele(), newVehicule.getNb_place());
			vehiculeValidator.validator();
			
			vehiculeService.create(newVehicule);
			response.sendRedirect(request.getContextPath() + "/cars");

		} catch (InputException e) {
			request.setAttribute("errorMessage", "Une erreur est survenue :" + e.getMessage());
			doGet(request, response);
		} catch (ServiceException e) {
			request.setAttribute("errorMessage", "Une erreur est survenue :" + e.getMessage());
			doGet(request, response);
		} 
		
	}

}
