package com.epf.RentManager.servletVehicule;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.RentManager.exception.ServiceException;
import com.epf.RentManager.service.VehiculeService;

@WebServlet("/cars")
public class VehiculeListServlet extends HttpServlet {
	
	VehiculeService vehiculeService = VehiculeService.getInstance();
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher ("/WEB-INF/views/vehicles/list.jsp");
		
		try {
			request.setAttribute("list_vehicules", vehiculeService.findAll());
		} catch (ServiceException e) {
			request.setAttribute("list_vehicules", "Une erreur est survenue" + e.getMessage());
		}
		dispatcher.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
