package com.epf.RentManager.servletVehicule;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.RentManager.exception.ServiceException;
import com.epf.RentManager.model.Reservation;
import com.epf.RentManager.service.ReservationService;
import com.epf.RentManager.service.VehiculeService;

@WebServlet("/cars/delete")
public class VehiculeDeleteServlet extends HttpServlet {
	
	VehiculeService vehiculeService = VehiculeService.getInstance();
	ReservationService resaService = ReservationService.getInstance();

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher ("/WEB-INF/views/vehicles/list.jsp");

		dispatcher.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			
			List<Reservation> listResa = resaService.findAll();
			
			for(Reservation resa : listResa) {
				if (resa.getVehicule_id() == id) {
					resaService.delete(resa.getId());
				}
			}
			
			vehiculeService.delete(id);
			response.sendRedirect(request.getContextPath() + "/cars");
		} catch (ServiceException e) {
			request.setAttribute("errorMessage", "Une erreur est survenue :" + e.getMessage());
			doGet(request, response);
		}

	}

}
