package com.epf.RentManager.model;

public class Vehicule {
	
	private int id;
	private String constructeur;
	private String modele;
	private byte nb_place;

	public Vehicule (){
		
	}
	
	public Vehicule (int id, String constructeur, String modele, byte nb_place) {
		this.id = id;
		this.constructeur = constructeur;
		this.modele = modele;
		this.nb_place = nb_place;
	}

	@Override
	public String toString() {
		return "Vehicule [id=" + id + ", constructeur=" + constructeur + ", modele=" + modele + ", nb_place=" + nb_place
				+ "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getConstructeur() {
		return constructeur;
	}

	public void setConstructeur(String constructeur) {
		this.constructeur = constructeur;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public byte getNb_place() {
		return nb_place;
	}

	public void setNb_place(byte nb_place) {
		this.nb_place = nb_place;
	}
	
}
