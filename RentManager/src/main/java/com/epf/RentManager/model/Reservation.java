package com.epf.RentManager.model;

import java.sql.Date;

public class Reservation {
	
	private int id;
	private Client client;
	private Vehicule vehicule;
	private Date debut;
	private Date fin;
	
	public Reservation () {
		
	}
	
	public Reservation (int id, Client client, Vehicule vehicule, Date debut, Date fin) {
		this.id = id;
		this.client = client;
		this.vehicule = vehicule;
		this.debut = debut;
		this.fin = fin;
	}
	
	@Override
	public String toString() {
		return "Reservation [id=" + id + ", client_id=" + client.getId() + ", client_prenom=" + client.getPrenom() + ", client_nom=" + client.getNom() + ", vehicule_id=" + vehicule.getId() + ", vehicule_constructeur=" + vehicule.getConstructeur() + ", vehicule_modele=" + vehicule.getModele() + ", debut="
				+ debut + ", fin=" + fin + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public Client getClient () {
		return client;
	}
	
	public void setClient (Client client) {
		this.client = client;
	}

	public int getClient_id() {
		return client.getId();
	}
	
	public void setClient_id(int client_id) {
		this.client.setId(client_id);
	}
	
	public Vehicule getVehicule() {
		return vehicule;
	}
	
	public void setVehicule (Vehicule vehicule) {
		this.vehicule = vehicule;
	}
	
	public int getVehicule_id() {
		return vehicule.getId();
	}

	public void setVehicule_id(int vehicule_id) {
		this.vehicule.setId(vehicule_id);
	}

	public Date getDebut() {
		return debut;
	}

	public void setDebut(Date debut) {
		this.debut = debut;
	}

	public Date getFin() {
		return fin;
	}

	public void setFin(Date fin) {
		this.fin = fin;
	}
	
	
	
}

	