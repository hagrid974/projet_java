package com.epf.RentManager.servletClient;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.RentManager.exception.ServiceException;
import com.epf.RentManager.model.Client;
import com.epf.RentManager.service.ClientService;

@WebServlet("/users/details")
public class ClientDetailsServlet extends HttpServlet {
	
	ClientService clientService = ClientService.getInstance();
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher ("/WEB-INF/views/users/details.jsp");
		
		int id = Integer.parseInt(request.getParameter("id"));
		
		//appel service
		try {
			Optional<Client> optClient = clientService.findById(id);
			if(optClient.isPresent()) {
				Client c = optClient.get();
				request.setAttribute("client", c); 
			}else {
				request.setAttribute("errorMessage", "Une erreur est survenue");
			}	
		} catch (ServiceException e) {
			request.setAttribute("errorMessage", "Une erreur est survenue :" + e.getMessage());
		}

		dispatcher.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
