package com.epf.RentManager.servletClient;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.RentManager.exception.ServiceException;
import com.epf.RentManager.service.ClientService;

@WebServlet("/users")
public class ClientListServlet extends HttpServlet {
	
	ClientService clientService = ClientService.getInstance();
	
	/**
	 * @see HttpServlet
	 */
	
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher ("/WEB-INF/views/users/list.jsp");
		
		try {
			request.setAttribute("list_clients", clientService.findAll());
		} catch (ServiceException e) {
			request.setAttribute("list_clients", "Une erreur est survenue" + e.getMessage());
		}
		dispatcher.forward(request, response);
	}
	
	/*protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		int client_ID = Integer.parseInt(request.getParameter("deleteid"));


		try {
			clientService.delete(client_ID);
			response.sendRedirect(request.getContextPath()+ "/users");
		} catch (ServiceException e) {
			request.setAttribute("errorMessage", "Une erreur est survenue :" + e.getMessage());

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/users/list.jsp");
			dispatcher.forward(request, response);
		}
	}

}
