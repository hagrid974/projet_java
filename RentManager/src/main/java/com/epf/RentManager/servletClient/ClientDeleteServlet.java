package com.epf.RentManager.servletClient;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.RentManager.exception.ServiceException;
import com.epf.RentManager.model.Reservation;
import com.epf.RentManager.service.ClientService;
import com.epf.RentManager.service.ReservationService;

@WebServlet("/users/delete")
public class ClientDeleteServlet extends HttpServlet {
	
	ClientService clientService = ClientService.getInstance();
	ReservationService resaService = ReservationService.getInstance();

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher ("/WEB-INF/views/users/list.jsp");

		dispatcher.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			
			List<Reservation> listResa = resaService.findAll();
			
			for(Reservation resa : listResa) {
				if (resa.getClient_id() == id) {
					resaService.delete(resa.getId());
				}
			}
			
			clientService.delete(id);
			response.sendRedirect(request.getContextPath() + "/users");
			
		} catch (ServiceException e) {
			request.setAttribute("errorMessage", "Une erreur est survenue :" + e.getMessage());
			doGet(request, response);
		}

	}

}
