package com.epf.RentManager.servletClient;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epf.RentManager.exception.InputException;
import com.epf.RentManager.exception.ServiceException;
import com.epf.RentManager.model.Client;
import com.epf.RentManager.service.ClientService;
import com.epf.RentManager.validator.ClientValidator;

@WebServlet("/users/create")
public class ClientAddServlet extends HttpServlet{
	
	ClientService clientService = ClientService.getInstance();
	
	
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher ("/WEB-INF/views/users/create.jsp");
		
		dispatcher.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			
			String last_name = request.getParameter("last_name");
			String first_name = request.getParameter("first_name");
			String email = request.getParameter("email");
			Date birth_date = Date.valueOf(request.getParameter("birth_date"));
			
			Client newClient = new Client();
			newClient.setNom(last_name);
			newClient.setPrenom(first_name);
			newClient.setEmail(email);
			newClient.setNaissance(birth_date);
			
			ClientValidator clientValidator = new ClientValidator(newClient.getId(), newClient.getNom(), newClient.getPrenom(), newClient.getEmail(), newClient.getNaissance());
			clientValidator.validator();
			
			clientService.create(newClient);
			response.sendRedirect(request.getContextPath() + "/users");

		} catch (InputException e) {
			request.setAttribute("errorMessage", "Une erreur est survenue :" + e.getMessage());
			doGet(request, response);
		} catch (ServiceException e) {
			request.setAttribute("errorMessage", "Une erreur est survenue :" + e.getMessage());
			doGet(request, response);
		} catch (IllegalArgumentException e) {
			request.setAttribute("errorMessage", "Une erreur est survenue : Veuillez saisir une date valide (yyyy-mm-dd)");
			doGet(request, response);
		}
		
		/* ou on aurait pu mettre la boucle try plus bas avant clientService.create(newClient) et apr�s avoir ajout� "RequestDispatcher dispatcher" 
		 * on aurait la boucle suivante: 
		 * try {
			clientService.create(newClient);
			response.sendRedirect(request.getContextPath()+ "/users");
		} catch (ServiceException e) {
			request.setAttribute("errorMessage", "Une erreur est survenue :" + e.getMessage());
		
			dispatcher = request.getRequestDispatcher("/WEB-INF/views/users/create.jsp");
			dispatcher.forward(request, response);
		}
		 * */
		
	}
}
