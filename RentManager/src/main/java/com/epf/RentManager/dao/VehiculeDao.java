package com.epf.RentManager.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.epf.RentManager.exception.DaoException;
import com.epf.RentManager.model.Vehicule;
import com.epf.RentManager.persistence.ConnectionManager;

public class VehiculeDao {
	
	private static VehiculeDao instance = null;
	private VehiculeDao() {}
	public static VehiculeDao getInstance() {
		if(instance == null) {
			instance = new VehiculeDao();
		}
		return instance;
	}
	
	private static final String CREATE_VEHICLE_QUERY = "INSERT INTO Vehicle(constructeur, modele, nb_places) VALUES(?, ?, ?);";
	private static final String DELETE_VEHICLE_QUERY = "DELETE FROM Vehicle WHERE id=?;";
	private static final String FIND_VEHICLE_QUERY = "SELECT id, constructeur, modele, nb_places FROM Vehicle WHERE id=?;";
	private static final String FIND_VEHICLES_QUERY = "SELECT id, constructeur, modele, nb_places FROM Vehicle;";
	
	
	public long create(Vehicule vehicule) throws DaoException {
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(CREATE_VEHICLE_QUERY);)
				{
				statement.setString(1, vehicule.getConstructeur());
				statement.setString(2, vehicule.getModele());
				statement.setByte(3, vehicule.getNb_place());
				
				long result = statement.executeUpdate();
				return result;
			} catch(SQLException e) {
				throw new DaoException("Erreur lors de la creation : " + e.getMessage());
			}
	}

	public long delete(int id) throws DaoException {
		
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(DELETE_VEHICLE_QUERY);){
			
			statement.setInt(1, id);
			
			long result = statement.executeUpdate();
			return result;
			
		} catch (SQLException e) {
			throw new DaoException("Erreur lors de la suppression : " + e.getMessage());
		} 
		
	}

	public Optional<Vehicule> findById(int id) {
		
		Vehicule v = new Vehicule();
		
		Optional<Vehicule> optVehicule = Optional.of(v);
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(FIND_VEHICLE_QUERY);) {
			
			statement.setInt(1, id);
			ResultSet resultSet2 = statement.executeQuery();
			
			while(resultSet2.next()) {
				
				v.setId(id);
				v.setConstructeur(resultSet2.getString(2));
				v.setModele(resultSet2.getString(3));
				v.setNb_place(resultSet2.getByte(4));
			}
			
		} catch(SQLException e) {
			return Optional.empty();
		}
		return optVehicule;
	}

	public List<Vehicule> findAll() throws DaoException {
		
		List<Vehicule> resultList = new ArrayList<>();
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(FIND_VEHICLES_QUERY);)
		{
			ResultSet resultSet = statement.executeQuery();
			
			while(resultSet.next()) {
			
			Vehicule vehicule = new Vehicule(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getByte(4));
			resultList.add(vehicule);
			}
			return resultList;
			
		} catch(SQLException e) {
			// e.printStackTrace(); Pour comprendre l'erreur
			throw new DaoException("Erreur lors du SELECT : " + e.getMessage());
		}
		
	}
	
	

}
