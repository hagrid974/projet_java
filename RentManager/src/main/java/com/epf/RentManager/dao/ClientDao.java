package com.epf.RentManager.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import com.epf.RentManager.exception.DaoException;
import com.epf.RentManager.model.Client;
import com.epf.RentManager.persistence.ConnectionManager;

public class ClientDao {
	
	private static ClientDao instance = null;
	private ClientDao() {}
	public static ClientDao getInstance() {
		if(instance == null) {
			instance = new ClientDao();
		}
		return instance;
	}
	
	private static final String CREATE_CLIENT_QUERY = "INSERT INTO Client(nom, prenom, email, naissance) VALUES(?, ?, ?, ?);";
	private static final String DELETE_CLIENT_QUERY = "DELETE FROM Client WHERE id=?;";
	private static final String FIND_CLIENT_QUERY = "SELECT id, nom, prenom, email, naissance FROM Client WHERE id=?;";
	private static final String FIND_CLIENTS_QUERY = "SELECT id, nom, prenom, email, naissance FROM Client;";
	
	public long create(Client client) throws DaoException {
		try (Connection conn = ConnectionManager.getConnection();
			PreparedStatement statement = conn.prepareStatement(CREATE_CLIENT_QUERY);)
			{
			statement.setString(1, client.getNom());
			statement.setString(2, client.getPrenom());
			statement.setString(3, client.getEmail());
			statement.setDate(4, client.getNaissance());
			
			long result = statement.executeUpdate();
			return result;
		} catch(SQLException e) {
			throw new DaoException("Erreur lors de la création : " + e.getMessage());
		}
	}

	/**
	 * Recupere tous les clients de la base
	 * @return Liste de clients
	 * @throws DaoException Si une exception SQLException est détectée lors de l'exécution 
	 */
	
	public List<Client> findAll() throws DaoException {
		List<Client> resultList = new ArrayList<>();
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(FIND_CLIENTS_QUERY);)
		{
			ResultSet resultSet = statement.executeQuery();
			
			while(resultSet.next()) {
			
			Client client = new Client(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getDate(5));
			
			/* Autre m�thode si on utilise le constructeur de base et non le constructeur vide 
			Client client2 = new Client();
			client2.setId(resultSet.getInt(1)); */
			
			resultList.add(client);
			}
			return resultList;
			
		} catch(SQLException e) {
			// e.printStackTrace(); Permet de comprendre l'erreur
			throw new DaoException("Erreur lors du SELECT : " + e.getMessage());
		}
	}
	
	/**
	 * Affiche la liste donnée en paramètre dans la console
	 * @param liste de clients
	 */
	
	private static void afficheListClient (List<Client> list) {
		for (Client c : list) {
			System.out.println(c);
		}
	}
	
	public Optional<Client> findById(int id){
		
		Client c = new Client();
		
		Optional<Client> optClient = Optional.of(c);
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(FIND_CLIENT_QUERY);) {
			
			statement.setInt(1, id);
			ResultSet resultSet2 = statement.executeQuery();
			
			while(resultSet2.next()) {
				
				c.setId(resultSet2.getInt(1));
				c.setNom(resultSet2.getString(2));
				c.setPrenom(resultSet2.getString(3));
				c.setEmail(resultSet2.getString(4));
				c.setNaissance(resultSet2.getDate(5));
			}
			
		} catch(SQLException e) {
			return Optional.empty();
		}
		return optClient;
	}
	
	public long delete(int id) throws DaoException {
	
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(DELETE_CLIENT_QUERY);){
			
			statement.setInt(1, id);
			
			long result = statement.executeUpdate();
			return result;
			
		} catch (SQLException e) {
			throw new DaoException("Erreur lors de la suppression : " + e.getMessage());
		} 
		
	}
	
	public static void main (String...args) {
		ClientDao dao = ClientDao.getInstance();
		
		try {
			List<Client> list = dao.findAll();
			
			/* Il vaut mieux d'appeler directement la methode
			for(Client c : list) {
				System.out.println(c);
			} */
			
			/* Autre maniere de l'ecrire 
			   for(int i = 0 ; i < list.size(); i++) {
			 
				System.out.println(list.get(i));
			} */
			
			afficheListClient(list);
			
		} catch (DaoException e) {
			System.out.println("Erreur lors du Select ALL : " + e.getMessage());
		}
		
		System.out.println("Veuillez rentrer l'ID du client que vous souhaitez obtenir :");
		
		Scanner sc = new Scanner (System.in);
		int id = sc.nextInt();
		
		Optional<Client> optClient = dao.findById(id);
		
		if(optClient.isPresent()) {
			Client c = optClient.get();
			System.out.println(c);
		}else {
			System.out.println("Erreur lors du select du client");
		}
		
		sc.close();
		
	}

}
