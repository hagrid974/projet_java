package com.epf.RentManager.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.epf.RentManager.exception.DaoException;
import com.epf.RentManager.model.Client;
import com.epf.RentManager.model.Reservation;
import com.epf.RentManager.model.Vehicule;
import com.epf.RentManager.persistence.ConnectionManager;

public class ReservationDao {

	private static ReservationDao instance = null;
	private ReservationDao() {}
	public static ReservationDao getInstance() {
		if(instance == null) {
			instance = new ReservationDao();
		}
		return instance;
	}
	
	private static final String CREATE_RESERVATION_QUERY = "INSERT INTO Reservation(client_id, vehicle_id, debut, fin) VALUES(?, ?, ?, ?);";
	private static final String DELETE_RESERVATION_QUERY = "DELETE FROM Reservation WHERE id=?;";
	private static final String FIND_RESERVATIONS_BY_CLIENT_QUERY = "SELECT r.id, r.client_id, c.prenom, c.nom, r.vehicle_id, v.constructeur, v.modele, r.debut, r.fin FROM Reservation AS r INNER JOIN Client AS c ON r.client_id=c.id INNER JOIN Vehicle AS v ON r.vehicle_id=v.id WHERE r.client_id=?;";
	private static final String FIND_RESERVATIONS_BY_VEHICLE_QUERY = "SELECT r.id, r.client_id, c.prenom, c.nom, r.vehicle_id, v.constructeur, v.modele, r.debut, r.fin FROM Reservation AS r INNER JOIN Client AS c ON r.client_id=c.id INNER JOIN Vehicle AS v ON r.vehicle_id=v.id WHERE r.vehicle_id=?;";
	private static final String FIND_RESERVATIONS_QUERY = "SELECT r.id, r.client_id, c.prenom, c.nom, r.vehicle_id, v.constructeur, v.modele, r.debut, r.fin FROM Reservation AS r INNER JOIN Client AS c ON r.client_id=c.id INNER JOIN Vehicle AS v ON r.vehicle_id=v.id ;";
		
	public long create(Reservation reservation) throws DaoException {
		
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(CREATE_RESERVATION_QUERY);)
				{
				try {
					statement.setInt(1, reservation.getClient_id());
				} catch (NullPointerException e) {
					throw new NullPointerException("L'ID du client n'existe pas");
				}
				try {
					statement.setInt(2, reservation.getVehicule_id());
				} catch (NullPointerException e) {
					throw new NullPointerException("L'ID du vehicule n'existe pas");
				}
				statement.setDate(3, reservation.getDebut());
				statement.setDate(4, reservation.getFin());
				
				long result = statement.executeUpdate();
				return result;
			} catch(SQLException e) {
				throw new DaoException("Erreur lors de la création : " + e.getMessage());
			}
		
	}
	
	public long delete(int id) throws DaoException {
		
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(DELETE_RESERVATION_QUERY);){
			
			statement.setInt(1, id);
			
			long result = statement.executeUpdate();
			return result;
			
		} catch (SQLException e) {
			throw new DaoException("Erreur lors de la suppression : " + e.getMessage());
		} 
		
	}

	
	public List<Reservation> findResaByClientId(int clientId) throws DaoException {
		
		List<Reservation> list = new ArrayList<>();

		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(FIND_RESERVATIONS_BY_CLIENT_QUERY);) {
			
			statement.setInt(1, clientId);
			ResultSet resultSet2 = statement.executeQuery();
			
			while(resultSet2.next()) {
				
				Client cli = new Client();
				cli.setId(resultSet2.getInt(2));
				cli.setPrenom(resultSet2.getString(3));
				cli.setNom(resultSet2.getString(4));
				
				Vehicule veh = new Vehicule();
				veh.setId(resultSet2.getInt(5));
				veh.setConstructeur(resultSet2.getString(6));
				veh.setModele(resultSet2.getString(7));
				
				
				Reservation r = new Reservation();
				
				r.setId(resultSet2.getInt(1));
				r.setClient(cli);
				r.setVehicule(veh);
				r.setDebut(resultSet2.getDate(8));
				r.setFin(resultSet2.getDate(9));
				list.add(r);						
				
			}
			
		} catch(SQLException e) {
			throw new DaoException("Erreur lors du SELECT de la réservation : " + e.getMessage());
		}
		
		return list;
	}
	
	public List<Reservation> findResaByVehicleId(int vehiculeId) throws DaoException {

		List<Reservation> list = new ArrayList<>();
		
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(FIND_RESERVATIONS_BY_VEHICLE_QUERY);) {
			
			statement.setInt(1, vehiculeId);
			ResultSet resultSet2 = statement.executeQuery();
			
			while(resultSet2.next()) {
				
				Client cli = new Client();
				cli.setId(resultSet2.getInt(2));
				cli.setPrenom(resultSet2.getString(3));
				cli.setNom(resultSet2.getString(4));
				
				Vehicule veh = new Vehicule();
				veh.setId(resultSet2.getInt(5));
				veh.setConstructeur(resultSet2.getString(6));
				veh.setModele(resultSet2.getString(7));
						
				Reservation r = new Reservation();
				
				r.setId(resultSet2.getInt(1));
				r.setClient(cli);
				r.setVehicule(veh);
				r.setDebut(resultSet2.getDate(8));
				r.setFin(resultSet2.getDate(9));
				list.add(r);			
					
			}
			
		} catch(SQLException e) {
			throw new DaoException("Erreur lors du SELECT de la réservation : " + e.getMessage());
		}	 
		
		return list;
	} 

	public List<Reservation> findAll() throws DaoException {
		
		List<Reservation> resultList = new ArrayList<>();
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(FIND_RESERVATIONS_QUERY);)
		{
			ResultSet resultSet = statement.executeQuery();
			
			while(resultSet.next()) {
				
				Client cli = new Client();
				cli.setId(resultSet.getInt(2));
				cli.setPrenom(resultSet.getString(3));
				cli.setNom(resultSet.getString(4));
				
				Vehicule vehicle = new Vehicule();
				vehicle.setId(resultSet.getInt(5));
				vehicle.setConstructeur(resultSet.getString(6));
				vehicle.setModele(resultSet.getString(7));
					
				Reservation reser = new Reservation(resultSet.getInt(1), cli, vehicle, resultSet.getDate(8), resultSet.getDate(9));
				
				resultList.add(reser);
					
			}
			return resultList;
			
		} catch(SQLException e) {
			// e.printStackTrace(); Pour comprendre l'erreur
			throw new DaoException("Erreur lors du SELECT : " + e.getMessage());
		} 
		 
	}
}
