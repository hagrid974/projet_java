class="col-sm-2 control-label"<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<%@include file="/WEB-INF/views/common/head.jsp"%>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 

    <%@ include file="/WEB-INF/views/common/header.jsp" %>
    <!-- Left side column. contains the logo and sidebar -->
    <%@ include file="/WEB-INF/views/common/sidebar.jsp" %>

 

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Utilisateurs
            </h1>
        </section>

 

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box">
                        <!-- form start -->
                        <!-- Le  type de methode http qui sera appel� lors de action submit du formulaire -->
                        <!-- est d�crit dans l'attribut "method" de la balise forme -->
                        <!-- action indique � quelle "cible" sera envoy�e la requ�te, ici notre Servlet qui sera bind sur -->
                        <!-- /vehicles/create -->
                        <form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/users/edit">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="nom" class="col-sm-2 control-label">Nom</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom" required value=${this_nom}>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="prenom" class="col-sm-2 control-label">Pr�nom</label>

 

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Pr�nom" required value=${this_prenom}>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-sm-2 control-label">Email</label>

 

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Email" required value=${this_email}>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="date_naissance" class="col-sm-2 control-label">Date de naissance</label>

 

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="date_naissance" name="date_naissance" placeholder="Date de naissance" required data-inputmask="'alias': 'yyyy-mm-dd'" data-mask value=${this_date_naissance}>
                                        
                                    </div>
                                </div>
                                
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Editer</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div>
                                	${errorMessage}
                                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

 

    <%@ include file="/WEB-INF/views/common/footer.jsp" %>
</div>
<!-- ./wrapper -->

 

<%@ include file="/WEB-INF/views/common/js_imports.jsp" %>
</body>
</html>
